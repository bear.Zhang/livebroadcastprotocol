import Vue from 'vue'
import Router from 'vue-router'

import protocolManageRputer from './modules/protocolManage'
import statusManageRputer from './modules/statusManage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HeaderTop',
      component: 
      () => import('@/components/common/headerTop'),
      redirect:'/homePage/homeShowIndex',
      children:[
        ...protocolManageRputer,
        ...statusManageRputer
      ]
    },
  ]
})
