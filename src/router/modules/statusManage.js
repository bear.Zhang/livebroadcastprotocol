export default [{
    path: '/statusManage',
    name: 'statusManage',
    component: () => import('@/components/statusManage'),
    redirect: '/statusManage/statusManageIndex',
    meta: {
      title: '状态管理中心'
    },
    children: [{
      path: '/statusManage/statusManageIndex',
      name: 'status',
      meta: {
        title: '状态管理'
      },
      hidden:false,
      component: () => import('@/components/statusManage/statusManage')
    }]
}]
  