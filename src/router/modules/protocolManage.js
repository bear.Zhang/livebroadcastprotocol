export default [{
    path: '/homePage',
    name: 'homePage',
    component: () => import('@/components/home/homePage'),
    redirect: '/homePage/homeShowIndex',
    meta: {
      title: '管理中心'
    },
    children: [{
      path: '/homePage/homeShowIndex',
      name: 'homeShow',
      meta: {
        title: '协议管理'
      },
      hidden:false,
      component: () => import('@/components/home/homeShow')
    }]
}]
  